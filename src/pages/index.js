import { Link } from "gatsby"
import React, { Fragment } from "react"
import BannerArea from "../sections/home/BannerArea"
import HeaderNav from "../sections/common/HeaderNav"
import AboutUsArea from "../sections/common/AboutUsArea"
import OurProductsArea from "../sections/home/OurProductsArea"
import TestimonialArea from "../sections/home/TestimonialArea"
import FAQArea from "../sections/home/FAQArea"
import BoxArea from "../sections/home/BoxArea"
import SeeProductArea from "../sections/home/SeeProductArea"
import ServiceArea from "../sections/home/ServiceArea"
import OfferArea from "../sections/home/OfferArea"
import OurGalleryArea from "../sections/home/OurGalleryArea"
import BlogArea from "../sections/home/BlogArea"
import SideBarNav from "../sections/common/SideBarNav"
import FooterArea from "../sections/common/FooterArea"
import Helmet from "react-helmet"

export default function Home() {

  return (
    <Fragment>

		<Helmet title="MA'MAGNI FOOD SARL - Alimentation saine" />
		<Helmet>
			<meta name="description" content="MA'MAGNI FOODS SARL entend renverser la tendance en proposant le concept 'Mangez sain et naturel' aux concommateurs "/>
		</Helmet>

      <HeaderNav />

      <SideBarNav />

      <BannerArea />

      <BoxArea />

      <AboutUsArea />

      <SeeProductArea />

      <OurProductsArea />

      <ServiceArea />

      <OfferArea />

      <OurGalleryArea />

      <TestimonialArea />

      <FAQArea />

      <BlogArea />

      <FooterArea />
    </Fragment>
  )
}
