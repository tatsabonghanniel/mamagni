import { Link } from "gatsby"
import React, { Fragment } from "react"

export default function NotFound() {
  return (
    <Fragment>
      <div className="error-area">
        <div className="d-table">
          <div className="d-table-cell">
            <div className="error-contant-wrap">
              <img src="assets/img/404.jpg" alt="404" />
              <h3>Oops! Page Not Found</h3>
              <p>The page you were looking for could not be found.</p>
              <Link to="/" className="default-btn">
                Return Home page
                <i className="flaticon-right"></i>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}
