import { Link } from "gatsby"
import React, { Fragment } from "react"
import AboutUsAreaTwo from "../sections/about-us/AboutUsAreaTwo"
import CounterArea from "../sections/about-us/CounterArea"
import PartnerArea from "../sections/about-us/PartnerArea"
import TeamArea from "../sections/about-us/TeamArea"
import TestimonialAreaTwo from "../sections/about-us/TestimonialAreaTwo"
import AboutUsArea from "../sections/common/AboutUsArea"
import FooterArea from "../sections/common/FooterArea"
import HeaderNav from "../sections/common/HeaderNav"
import PageTitleArea from "../sections/common/PageTitleArea"
import SideBarNav from "../sections/common/SideBarNav"
import SubscribeArea from "../sections/common/SubscribeArea"

import Helmet from "react-helmet"


export default function AboutUs() {
  return (
    <Fragment>
      <Helmet title="MA'MAGNI FOOD SARL - A propos" />
      <Helmet>
        <meta
          name="description"
          content="MA'MAGNI FOODS SARL entend renverser la tendance en proposant le concept 'Mangez sain et naturel' aux concommateurs "
        />
      </Helmet>

      <HeaderNav />

      <SideBarNav />

      <PageTitleArea />

      <AboutUsArea />

      <CounterArea />

      <AboutUsAreaTwo />

      <TeamArea />

      <TestimonialAreaTwo />

      <PartnerArea />

      <SubscribeArea />

      <FooterArea />
    </Fragment>
  )
}
