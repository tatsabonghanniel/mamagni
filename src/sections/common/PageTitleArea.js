import React from "react"

export default function PageTitleArea() {
  return (
    <div className="page-title-area item-bg-1">
      <div className="container">
        <div className="page-title-content">
          <h2>About Us</h2>
          <ul>
            <li>
              <a href="index.html">
                Home
                <i className="fa fa-chevron-right"></i>
              </a>
            </li>
            <li>About Us</li>
          </ul>
        </div>
      </div>
    </div>
  )
}
