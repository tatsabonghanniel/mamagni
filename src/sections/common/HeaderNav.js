import { Link } from "gatsby"
import React, { useCallback, useEffect, Fragment } from "react"
import { ABOUT_US_PAGE, HOME_PAGE } from "../../Routes"

import { Location } from "@reach/router"

import Helmet from "react-helmet"

const HeaderNav = () => {
  return (
    <Location>
      {locationProps => {
        const CurrentURL = locationProps.location.pathname

        return (
          <Fragment>
            <Helmet>
              <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
              <link
                rel="stylesheet"
                href="/assets/css/owl.theme.default.min.css"
              />
              <link rel="stylesheet" href="/assets/css/owl.carousel.min.css" />
              <link rel="stylesheet" href="/assets/css/animate.css" />
              <link rel="stylesheet" href="/assets/css/flaticon.css" />
              <link rel="stylesheet" href="/assets/css/font-awesome.min.css" />
              <link rel="stylesheet" href="/assets/css/meanmenu.css" />
              <link rel="stylesheet" href="/assets/css/nice-select.css" />
              <link rel="stylesheet" href="/assets/css/imagelightbox.min.css" />
              <link rel="stylesheet" href="/assets/css/odometer.css" />
              <link rel="stylesheet" href="/assets/css/style.css" />
              <link rel="stylesheet" href="/assets/css/responsive.css" />
            </Helmet>

            <div className="rimu-nav-style fixed-top">
              <div className="navbar-area">
                {/* <!-- Menu For Mobile Device --> */}
                <div className="mobile-nav">
                  <a href="index.html" className="logo">
                    <img src="assets/img/logo.png" alt="Ma'magni Logo" />
                  </a>
                </div>

                {/* <!-- Menu For Desktop Device --> */}
                <div className="main-nav">
                  <nav className="navbar navbar-expand-md navbar-light">
                    <div className="container">
                      <a className="navbar-brand" href="index.html">
                        <img
                          style={{ width: "110px", height: "30px" }}
                          src="assets/img/logo.png"
                          alt="Ma'magni Logo"
                        />
                      </a>

                      <div
                        className="collapse navbar-collapse mean-menu"
                        id="navbarSupportedContent"
                      >
                        <ul className="navbar-nav ml-auto">
                          <li className="nav-item">
                            <Link
                              to={HOME_PAGE}
                              className={
                                CurrentURL == HOME_PAGE
                                  ? "nav-link active"
                                  : "nav-link"
                              }
                            >
                              Accueil
                            </Link>
                          </li>

                          <li className="nav-item">
                            <Link
                              to={ABOUT_US_PAGE}
                              className={
                                CurrentURL == ABOUT_US_PAGE
                                  ? "nav-link active"
                                  : "nav-link"
                              }
                            >
                              A propos
                            </Link>
                          </li>

                          <li className="nav-item">
                            <a href="#" className="nav-link dropdown-toggle">
                              Services
                              <i className="flaticon-add-1"></i>
                            </a>
                            <ul className="dropdown-menu dropdown-style">
                              <li className="nav-item">
                                <a href="services.html" className="nav-link">
                                  Services
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="services-grid.html"
                                  className="nav-link"
                                >
                                  Services Grid
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="service-details.html"
                                  className="nav-link"
                                >
                                  Service Details
                                </a>
                              </li>
                            </ul>
                          </li>

                          <li className="nav-item">
                            <a href="#" className="nav-link dropdown-toggle">
                              Shop
                              <i className="flaticon-add-1"></i>
                            </a>
                            <ul className="dropdown-menu">
                              <li className="nav-item">
                                <a href="cart.html" className="nav-link">
                                  Cart
                                </a>
                              </li>
                              <li className="nav-item">
                                <a href="checkout.html" className="nav-link">
                                  Checkout
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="shop-grid-view.html"
                                  className="nav-link"
                                >
                                  Shop Grid View
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="shop-list-view.html"
                                  className="nav-link"
                                >
                                  Shop List View
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="shop-filter-view.html"
                                  className="nav-link"
                                >
                                  Shop Filter View
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="product-details.html"
                                  className="nav-link"
                                >
                                  Product Details
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="product-left-sidebar.html"
                                  className="nav-link"
                                >
                                  Product Left Sidebar
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="product-right-sidebar.html"
                                  className="nav-link"
                                >
                                  Product Right Sidebar
                                </a>
                              </li>
                            </ul>
                          </li>

                          <li className="nav-item">
                            <a href="#" className="nav-link dropdown-toggle">
                              Blog
                              <i className="flaticon-add-1"></i>
                            </a>
                            <ul className="dropdown-menu dropdown-style">
                              <li className="nav-item">
                                <a href="blog-grid.html" className="nav-link">
                                  Blog Grid
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="blog-right-sidebar.html"
                                  className="nav-link"
                                >
                                  Blog Right Sidebar
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="blog-left-sidebar.html"
                                  className="nav-link"
                                >
                                  Blog Left Sidebar
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="blog-details.html"
                                  className="nav-link"
                                >
                                  Blog Details
                                </a>
                              </li>
                            </ul>
                          </li>

                          <li className="nav-item">
                            <a href="contact.html" className="nav-link">
                              Contact
                            </a>
                          </li>
                        </ul>

                        <div className="others-option">
                          <a className="cart-icon" href="cart.html">
                            <i className="flaticon-shopping-bag"></i>
                            <span>3</span>
                          </a>
                          <div className="search-box-item">
                            <i className="search-btn fa fa-search"></i>
                            <i className="close-btn fa fa-close"></i>
                            <div className="search-overlay search-popup">
                              <div className="search-box">
                                <form className="search-form">
                                  <input
                                    className="search-input"
                                    name="search"
                                    placeholder="Search"
                                    type="text"
                                  />
                                  <button
                                    className="search-button"
                                    type="submit"
                                  >
                                    <i className="fa fa-search"></i>
                                  </button>
                                </form>
                              </div>
                            </div>
                          </div>
                          <a
                            className="sidebar-menu"
                            href="#"
                            data-toggle="modal"
                            data-target="#myModal2"
                          >
                            <i className="fa fa-bars"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                  </nav>
                </div>
              </div>
            </div>
          </Fragment>
        )
      }}
    </Location>
  )
}

export default HeaderNav
