import React from "react"
import { Fragment } from "react"

const FooterArea = () => {
  return (
    <Fragment>
      {/* <!-- Start Footer Top Area --> */}
      <section className="footer-top-area pt-100 pb-70">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-sm-6 col-md-6">
              <div className="single-widget">
                <a href="index.html">
                  <img style={{width: "110px", height: "30px"}} src="assets/img/logo.png" alt="Ma'magni-Logo" />
                </a>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Velit cupiditate ad quisquam ratione, unde exercitationem
                  ducimus.
                </p>
                <ul className="address">
                  <li>
                    <i className="fa fa-map-marker"></i>
                    123, Western Road, Melbourne Australia
                  </li>
                  <li>
                    <i className="fa fa-envelope"></i>
                    <a href="mailto:hello@rimu.com">hello@rimu.com</a>
                  </li>
                  <li>
                    <i className="fa fa-phone"></i>
                    <a href="tel:+123(456)123">+123(456)123</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-2 col-sm-6 col-md-6">
              <div className="single-widget">
                <h3>Information</h3>
                <ul className="links">
                  <li>
                    <a href="index.html">Delivery Information</a>
                  </li>
                  <li>
                    <a href="service.html">Secure Payment</a>
                  </li>
                  <li>
                    <a href="about.html">Discount</a>
                  </li>
                  <li>
                    <a href="testimonial.html">Top Sellers</a>
                  </li>
                  <li>
                    <a href="blog.html">Blog</a>
                  </li>
                  <li>
                    <a href="contact.html">Contact Us</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-2 col-sm-6 col-md-6">
              <div className="single-widget">
                <h3>Customer Support</h3>
                <ul className="links">
                  <li>
                    <a href="contact.html">Help & Ordering</a>
                  </li>
                  <li>
                    <a href="#">Order Tracking</a>
                  </li>
                  <li>
                    <a href="#">Return & Cancelation</a>
                  </li>
                  <li>
                    <a href="#">Delivery Schedule</a>
                  </li>
                  <li>
                    <a href="#">Get a Call</a>
                  </li>
                  <li>
                    <a href="#">Online Enquiry</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-4 col-sm-6 col-md-6">
              <div className="single-widget single-widget-4">
                <h3>Instagram</h3>
                <ul className="instagram">
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/1.jpg" alt="Instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/2.jpg" alt="Instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/3.jpg" alt="Instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/4.jpg" alt="Instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/5.jpg" alt="Instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/6.jpg" alt="Instagram" />
                    </a>
                  </li>
                </ul>
                <ul className="social-icon">
                  <li>
                    <a href="#">
                      <i className="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-linkedin"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-pinterest-p"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- End Footer Top Area --> */}

      {/* <!-- Start Footer Bottom Area --> */}
      <footer className="footer-bottom-area">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-6">
              <div>
                <p>Copyright © Ma'amagni. All Rights Reserved</p>
              </div>
            </div>
            <div className="col-lg-6 col-md-6">
              <ul className="conditions">
                <li>
                  <a href="#">Terms & Conditions</a>
                </li>
                <li>
                  <a href="#">Privacy Policy</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
      {/* <!-- End Footer Bottom Area --> */}

      {/* <!-- Start Go Top Area --> */}
      <div className="go-top">
        <i className="fa fa-angle-double-up"></i>
        <i className="fa fa-angle-double-up"></i>
      </div>
      {/* <!-- End Go Top Area --> */}
    </Fragment>
  )
}

export default FooterArea
