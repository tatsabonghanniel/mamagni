import React from "react"

export default function SubscribeArea() {
  return (
    <section className="subscribe-area ptb-100">
      <div className="container">
        <div className="section-title">
          <span>Subscribe Now</span>
          <h2>Subscribe Our Newsletter</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
            ipsum suspendisse ultrices gravida.
          </p>
        </div>
        <div className="row">
          <div className="col-12">
            <form className="newsletter-form">
              <input
                type="email"
                className="input-newsletter"
                placeholder="Enter your email"
                name="EMAIL"
                required
                autocomplete="off"
              />
              <button type="submit">Subscribe Now</button>
            </form>
          </div>
        </div>
      </div>
    </section>
  )
}
