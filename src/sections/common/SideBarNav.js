import React from "react"

const SideBarNav = () => {
  return (
    <div className="sidebar-modal">
      <div
        className="modal right fade"
        id="myModal2"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel2"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">
                  <i className="fa fa-times"></i>
                </span>
              </button>
              <h2 className="modal-title" id="myModalLabel2">
                <a href="index.html">
                  <img src="assets/img/logo.png" alt="Ma'magni Logo" />
                </a>
              </h2>
            </div>
            <div className="modal-body">
              <div className="sidebar-modal-widget">
                <h3 className="title">Additional Links</h3>
                <ul>
                  <li>
                    <a href="log-in.html">Log In</a>
                  </li>
                  <li>
                    <a href="sign-up.html">Sign Up</a>
                  </li>
                  <li>
                    <a href="faq.html">FAQ</a>
                  </li>
                  <li>
                    <a href="#">Logout</a>
                  </li>
                </ul>
              </div>
              <div className="sidebar-modal-widget">
                <h3 className="title">Contact Info</h3>
                <ul className="contact-info">
                  <li>
                    <i className="fa fa-map-marker"></i>
                    Address
                    <span>123,western Road, Melbourne Australia</span>
                  </li>
                  <li>
                    <i className="fa fa-envelope"></i>
                    Email
                    <span>hello@rimu.com</span>
                  </li>
                  <li>
                    <i className="fa fa-phone"></i>
                    Phone
                    <span>+123(456)123 +123(456)123</span>
                  </li>
                </ul>
              </div>
              <div className="sidebar-modal-widget">
                <h3 className="title">Connect With Us</h3>
                <ul className="social-list">
                  <li>
                    <a href="#">
                      <i className="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-instagram"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-linkedin"></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="sidebar-modal-widget">
                <h3 className="title">Instagram</h3>
                <ul className="instagram">
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/1.jpg" alt="Instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/2.jpg" alt="Instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/3.jpg" alt="Instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/4.jpg" alt="Instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/5.jpg" alt="Instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <img src="assets/img/instagram/6.jpg" alt="Instagram" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SideBarNav;