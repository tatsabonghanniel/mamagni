import React from "react"

const AboutUsArea = () => {
  return (
    <section className="about-us-area ptb-100">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-6">
            <div
              className="about-img-1 about-img-2 wow bounceInDown"
              data-wow-delay=".1s"
            >
              <img src="assets/img/about-1.png" alt="" />
            </div>
          </div>
          <div className="col-lg-6">
            <div className="about-content">
              <span>A propos de nous</span>
              <h2>Nous fournissons la nourriture fraiche pour votre famille</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
                ipsum suspendisse ultrices gravida.
              </p>
              <ul>
                <li>
                  <i className="flaticon-check-mark"></i>
                  Qualité supérieure
                </li>
                <li>
                  <i className="flaticon-check-mark"></i>
                  Récolte tous les jours
                </li>
                <li>
                  <i className="flaticon-check-mark"></i>
                  100% Biologique
                </li>
                <li>
                  <i className="flaticon-check-mark"></i>
                  Livraison rapide
                </li>
              </ul>
              <a className="default-btn default-btn-2" href="#">
                Apprendre plus
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="shape shape-1">
        <img src="assets/img/shape/3.png" alt="Shape" />
      </div>
      <div className="shape shape-2">
        <img src="assets/img/shape/4.png" alt="Shape" />
      </div>
      <div className="shape shape-3">
        <img src="assets/img/shape/5.png" alt="Shape" />
      </div>
      <div className="shape shape-4">
        <img src="assets/img/shape/6.png" alt="Shape" />
      </div>
      <div className="shape shape-5">
        <img src="assets/img/shape/7.png" alt="Shape" />
      </div>
    </section>
  )
}

export default AboutUsArea
