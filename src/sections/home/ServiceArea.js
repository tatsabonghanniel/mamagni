import React from "react"

export default function ServiceArea() {
  return (
    <section className="service-area pt-100 pb-70">
      <div className="container-fluid">
        <div className="section-title">
          <span>Services</span>
          <h2>Nous offrons les meilleurs services</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
            ipsum suspendisse ultrices gravida.
          </p>
        </div>
        <div className="row">
          <div className="col-12 p-0">
            <div className="service-wrap owl-carousel owl-theme">
              <div className="single-service">
                <img src="assets/img/services/1.jpg" alt="Services" />
                <div className="service-content">
                  <h3>Poissons frais</h3>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do
                  </p>
                  <a className="read-more" href="service-details.html">
                    Lire plus
                    <i className="flaticon-right"></i>
                  </a>
                </div>
              </div>
              <div className="single-service">
                <img src="assets/img/services/2.jpg" alt="Services" />
                <div className="service-content">
                  <h3>Aliments naturels</h3>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do
                  </p>
                  <a className="read-more" href="service-details.html">
                    Lire plus
                    <i className="flaticon-right"></i>
                  </a>
                </div>
              </div>
              <div className="single-service">
                <img src="assets/img/services/3.jpg" alt="Services" />
                <div className="service-content">
                  <h3>Lait concentré naturel</h3>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do
                  </p>
                  <a className="read-more" href="service-details.html">
                    Lire plus
                    <i className="flaticon-right"></i>
                  </a>
                </div>
              </div>
              <div className="single-service">
                <img src="assets/img/services/4.jpg" alt="Services" />
                <div className="service-content">
                  <h3>Viande fraiche</h3>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do
                  </p>
                  <a className="read-more" href="service-details.html">
                    Lire plus
                    <i className="flaticon-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="shape shape-1">
        <img src="assets/img/shape/2.png" alt="Shape" />
      </div>
    </section>
  )
}
