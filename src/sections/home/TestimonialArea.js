import React from "react"

const TestimonialArea = () => {
  return (
    <section className="testimonial-area ptb-100">
      <div className="container">
        <div className="section-title">
          <span>Témoignages</span>
          <h2>Que disent nos clients ?</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
            ipsum suspendisse ultrices gravida.
          </p>
        </div>
        <div className="row testimonial-bg-color">
          <div className="col-lg-6 col-md-6 p-0">
            <div className="client-img">
              <img src="assets/img/testimonial/6.jpg" alt="" />
              <div className="video-wrap">
                <div className="video-btn-wrap">
                  <a
                    href="play-video"
                    className="video-btn"
                    data-ilb2-video='{"controls":"controls", "autoplay":false, "sources":[{"src":"assets/img/video.mp4", "type":"video/mp4"}]}'
                    data-imagelightbox="video"
                  >
                    <i className="fa fa-play"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-6 p-0">
            <div className="testimonials-wrap owl-carousel owl-theme">
              <div className="single-testimonial">
                <img src="assets/img/testimonial/1.jpg" alt="" />
                <h3>Amelia Daniel</h3>
                <span>Chairman and founder</span>
                <i className="flaticon-quote"></i>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore dolore magna aliqua.
                  Quis ipsum suspendisse ultrices gravida. Risus viverra
                  maecenas accumsan lacus vel facilisis.
                </p>
                <ul>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                </ul>
              </div>
              <div className="single-testimonial">
                <img src="assets/img/testimonial/2.jpg" alt="" />
                <h3>Alex Mason</h3>
                <span>Visual Media</span>
                <i className="flaticon-quote"></i>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore dolore magna aliqua.
                  Quis ipsum suspendisse ultrices gravida. Risus viverra
                  maecenas accumsan lacus vel facilisis.
                </p>
                <ul>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                </ul>
              </div>
              <div className="single-testimonial">
                <img src="assets/img/testimonial/3.jpg" alt="" />
                <h3>Michael Harper</h3>
                <span>Sales Manager</span>
                <i className="flaticon-quote"></i>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore dolore magna aliqua.
                  Quis ipsum suspendisse ultrices gravida. Risus viverra
                  maecenas accumsan lacus vel facilisis.
                </p>
                <ul>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default TestimonialArea
