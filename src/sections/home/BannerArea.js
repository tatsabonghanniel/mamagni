import React from "react"

const BannerArea = () => {
  return (
    <section className="banner-area banner-area-five">
      <div className="container">
        <div className="row">
          <div className="col-lg-7">
            <div className="banner-text">
              <span className="wow fadeInUp" data-wow-delay=".1s">
                Extra 50% pour tous les plats d'hiver
              </span>
              <h1 className="wow fadeInUp" data-wow-delay=".3s">
                Vivre avec des aliments biologiques, vivre sainement
              </h1>
              <p className="wow fadeInUp" data-wow-delay=".6s">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Necessitatibus nobis labore quibusdam ad cum, doloremque dicta
                optio.
              </p>
              <div className="banner-btn wow fadeInUp" data-wow-delay=".9s">
                <a className="default-btn" href="#">
                  Nos Services
                </a>
                {/* 
                <div className="video-wrap">
                  <div className="video-btn-wrap">
                    <a
                      href="play-video"
                      className="video-btn"
                      data-ilb2-video='{"controls":"controls", "autoplay":false, "sources":[{"src":"assets/img/video.mp4", "type":"video/mp4"}]}'
                      data-imagelightbox="video"
                    >
                      <i className="fa fa-play"></i>
                    </a>
                  </div>
                  <span className="watch-video">Watch Video</span>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="shape shape-1">
        <img src="assets/img/banner/5.png" alt="Shape" />
      </div>
      <div className="shape shape-2">
        <img src="assets/img/banner/4.png" alt="Shape" />
      </div>
      <div className="shape shape-6">
        <img src="assets/img/banner/6.png" alt="Shape" />
      </div>
      <div className="shape shape-7">
        <img src="assets/img/banner/7.png" alt="Shape" />
      </div>
    </section>
  )
}

export default BannerArea
