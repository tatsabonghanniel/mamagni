import React from "react"

export default function OfferArea() {
  return (
    <section className="offer-area ptb-100">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-4">
            <div className="offer-logo">
              <p>50%</p>
              <span>réduction</span>
            </div>
          </div>
          <div className="col-lg-8">
            <div className="offer-title">
              <span>Deal of The Day</span>
              <h2>We offer a bit less at Midday</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
                ipsum suspendisse ultrices gravida.
              </p>
              <a className="default-btn" href="shop.html">
                Acheter maintenant
                <i className="flaticon-next"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
