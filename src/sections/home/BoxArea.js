import React from "react"

export default function BoxArea() {
  return (
    <section className="box-area pt-30 pb-70">
      <div className="container">
        <div className="row">
          <div className="col-lg-4 col-md-6">
            <div className="single-box">
              <div className="box-icon">
                <i className="flaticon-pumkin"></i>
              </div>
              <h3>Aliments naturels</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore.
              </p>
              <a className="read-more" href="#">
                Lire plus
                <i className="flaticon-right"></i>
              </a>
              <div className="shape-3">
                <img src="assets/img/shape/3.png" alt="Shape" />
              </div>
              <div className="shape-4">
                <img src="assets/img/shape/10.png" alt="Shape" />
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 icon-color">
            <div className="single-box">
              <div className="box-icon">
                <i className="flaticon-plant"></i>
              </div>
              <h3>Biologiquement sûr</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore.
              </p>
              <a className="read-more" href="#">
                Lire plus
                <i className="flaticon-right"></i>
              </a>
              <div className="shape-3">
                <img src="assets/img/shape/3.png" alt="Shape" />
              </div>
              <div className="shape-4">
                <img src="assets/img/shape/10.png" alt="Shape" />
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 offset-md-3 offset-lg-0 icon-color">
            <div className="single-box">
              <div className="box-icon">
                <i className="flaticon-juice"></i>
              </div>
              <h3>100% Jus naturels</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore.
              </p>
              <a className="read-more" href="#">
                Lire plus
                <i className="flaticon-right"></i>
              </a>
              <div className="shape-3">
                <img src="assets/img/shape/3.png" alt="Shape" />
              </div>
              <div className="shape-4">
                <img src="assets/img/shape/10.png" alt="Shape" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="shape shape-1">
        <img src="assets/img/shape/8.png" alt="" />
      </div>
      <div className="shape shape-2">
        <img src="assets/img/shape/9.png" alt="" />
      </div>
    </section>
  )
}
