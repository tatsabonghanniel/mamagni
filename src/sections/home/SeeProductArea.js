import React from 'react';

export default function SeeProductArea() {
    return (
        <section className="see-product-area ptb-100">
        <div className="container">
          <div className="section-title">
            <span>Tous les aliments</span>
            <h2>Il s'agit d'un mode de vie plus sain mais de manière naturelle</h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Asperiores dignissimos libero molestiae harum dolores numquam
              velit iste modi optio.
            </p>
            <a className="default-btn" href="#">
              Voir tous les aliments
            </a>
          </div>
        </div>
      </section>
    );
}