import React from "react"

const OurProductsArea = () => {
  return (
    <section className="our-product-area pt-100 pb-70">
      <div className="container">
        <div className="section-title">
          <span>Our Foods</span>
          <h2>Featured Foods</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
            ipsum suspendisse ultrices gravida.
          </p>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="shorting-menu">
              <button className="filter" data-filter="all">
                <i className="flaticon-grid"></i> All
              </button>
              <button className="filter" data-filter=".vegetables">
                Vegetables
              </button>
              <button className="filter" data-filter=".fruits">
                Fruits
              </button>
              <button className="filter" data-filter=".pumpkin">
                Pumpkin
              </button>
              <button className="filter" data-filter=".orange">
                Orange
              </button>
              <button className="filter" data-filter=".lemon">
                Lemon
              </button>
            </div>
          </div>
          <div className="shorting">
            <div className="row">
              <div
                className="col-lg-3 col-md-6 col-sm-6 mix vegetables fruits"
                data-bound
              >
                <div className="single-product-box">
                  <div className="product-image">
                    <img src="assets/img/product/2.jpg" alt="image" />
                    <div className="btn-box">
                      <a href="#">
                        <i className="flaticon-shopping-cart"></i>
                      </a>
                      <a href="#" className="link-btn">
                        <i className="flaticon-heart"></i>
                      </a>
                      <a
                        href="assets/img/product/2.jpg"
                        data-imagelightbox="popup-btn"
                        className="link-btn"
                      >
                        <i className="flaticon-magnifying-glass"></i>
                      </a>
                    </div>
                  </div>
                  <div className="product-content">
                    <h3>Fresh Strawberry</h3>
                    <ul>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                    </ul>
                    <span>$60</span>
                  </div>
                  <span className="product-offer">SALE</span>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 mix pumpkin orange lemon">
                <div className="single-product-box">
                  <div className="product-image">
                    <img src="assets/img/product/1.jpg" alt="image" />
                    <div className="btn-box">
                      <a href="#">
                        <i className="flaticon-shopping-cart"></i>
                      </a>
                      <a href="#" className="link-btn">
                        <i className="flaticon-heart"></i>
                      </a>
                      <a
                        href="assets/img/product/1.jpg"
                        data-imagelightbox="popup-btn"
                        className="link-btn"
                      >
                        <i className="flaticon-magnifying-glass"></i>
                      </a>
                    </div>
                  </div>
                  <div className="product-content">
                    <h3>Fresh Cucumber</h3>
                    <ul>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                    </ul>
                    <span>$60</span>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 mix pumpkin vegetables lemon">
                <div className="single-product-box">
                  <div className="product-image">
                    <img src="assets/img/product/3.jpg" alt="image" />
                    <div className="btn-box">
                      <a href="#">
                        <i className="flaticon-shopping-cart"></i>
                      </a>
                      <a href="#" className="link-btn">
                        <i className="flaticon-heart"></i>
                      </a>
                      <a
                        href="assets/img/product/3.jpg"
                        data-imagelightbox="popup-btn"
                        className="link-btn"
                      >
                        <i className="flaticon-magnifying-glass"></i>
                      </a>
                    </div>
                  </div>
                  <div className="product-content">
                    <h3>Fresh Grapes</h3>
                    <ul>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                    </ul>
                    <span>$60</span>
                  </div>
                  <span className="product-offer offer-10">-10%</span>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 mix orange fruits">
                <div className="single-product-box">
                  <div className="product-image">
                    <img src="assets/img/product/4.jpg" alt="image" />
                    <div className="btn-box">
                      <a href="#">
                        <i className="flaticon-shopping-cart"></i>
                      </a>
                      <a href="#" className="link-btn">
                        <i className="flaticon-heart"></i>
                      </a>
                      <a
                        href="assets/img/product/4.jpg"
                        data-imagelightbox="popup-btn"
                        className="link-btn"
                      >
                        <i className="flaticon-magnifying-glass"></i>
                      </a>
                    </div>
                  </div>
                  <div className="product-content">
                    <h3>Fresh Orange</h3>
                    <ul>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                    </ul>
                    <span>$60</span>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 mix fruits pumpkin ">
                <div className="single-product-box">
                  <div className="product-image">
                    <img src="assets/img/product/5.jpg" alt="image" />
                    <div className="btn-box">
                      <a href="#">
                        <i className="flaticon-shopping-cart"></i>
                      </a>
                      <a href="#" className="link-btn">
                        <i className="flaticon-heart"></i>
                      </a>
                      <a
                        href="assets/img/product/5.jpg"
                        data-imagelightbox="popup-btn"
                        className="link-btn"
                      >
                        <i className="flaticon-magnifying-glass"></i>
                      </a>
                    </div>
                  </div>
                  <div className="product-content">
                    <h3>Fresh Juices</h3>
                    <ul>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                    </ul>
                    <span>$60</span>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 mix orange vegetables lemon">
                <div className="single-product-box">
                  <div className="product-image">
                    <img src="assets/img/product/6.jpg" alt="image" />
                    <div className="btn-box">
                      <a href="#">
                        <i className="flaticon-shopping-cart"></i>
                      </a>
                      <a href="#" className="link-btn">
                        <i className="flaticon-heart"></i>
                      </a>
                      <a
                        href="assets/img/product/6.jpg"
                        data-imagelightbox="popup-btn"
                        className="link-btn"
                      >
                        <i className="flaticon-magnifying-glass"></i>
                      </a>
                    </div>
                  </div>

                  <div className="product-content">
                    <h3>Fresh Banana</h3>
                    <ul>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                    </ul>
                    <span>$60</span>
                  </div>
                  <span className="product-offer hot-offer">HOT</span>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 mix orange vegetables fruits">
                <div className="single-product-box">
                  <div className="product-image">
                    <img src="assets/img/product/7.jpg" alt="image" />
                    <div className="btn-box">
                      <a href="#">
                        <i className="flaticon-shopping-cart"></i>
                      </a>
                      <a href="#" className="link-btn">
                        <i className="flaticon-heart"></i>
                      </a>
                      <a
                        href="assets/img/product/7.jpg"
                        data-imagelightbox="popup-btn"
                        className="link-btn"
                      >
                        <i className="flaticon-magnifying-glass"></i>
                      </a>
                    </div>
                  </div>

                  <div className="product-content">
                    <h3>Fresh Tomato</h3>
                    <ul>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                    </ul>
                    <span>$60</span>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 mix lemon vegetables pumpkin">
                <div className="single-product-box">
                  <div className="product-image">
                    <img src="assets/img/product/8.jpg" alt="image" />
                    <div className="btn-box">
                      <a href="#">
                        <i className="flaticon-shopping-cart"></i>
                      </a>
                      <a href="#" className="link-btn">
                        <i className="flaticon-heart"></i>
                      </a>
                      <a
                        href="assets/img/product/8.jpg"
                        data-imagelightbox="popup-btn"
                        className="link-btn"
                      >
                        <i className="flaticon-magnifying-glass"></i>
                      </a>
                    </div>
                  </div>
                  <div className="product-content">
                    <h3>Fresh Carrots</h3>
                    <ul>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                      <li>
                        <i className="fa fa-star"></i>
                      </li>
                    </ul>
                    <span>$60</span>
                  </div>
                  <span className="product-offer">SALE</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default OurProductsArea
