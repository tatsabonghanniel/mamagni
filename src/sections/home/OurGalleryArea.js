import React from "react"

export default function OurGalleryArea() {
  return (
    <section className="our-gallery-area  ptb-100">
      <div className="container-fluid">
        <div className="section-title">
          <span>Aliments</span>
          <h2>Our Gallery</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
            ipsum suspendisse ultrices gravida.
          </p>
        </div>
        <div className="row">
          <div className="col-12 p-0">
            <div className="gallery-wrap owl-carousel owl-theme">
              <div className="single-gallery">
                <img src="assets/img/gallery/1.jpg" alt="Services" />
                <div className="gallery-content">
                  <h3>
                    <a href="shop.html">Strawberry Garden</a>
                  </h3>
                </div>
              </div>
              <div className="single-gallery">
                <img src="assets/img/gallery/2.jpg" alt="Services" />
                <div className="gallery-content">
                  <h3>
                    <a href="shop.html">Strawberry Garden</a>
                  </h3>
                </div>
              </div>
              <div className="single-gallery">
                <img src="assets/img/gallery/3.jpg" alt="Services" />
                <div className="gallery-content">
                  <h3>
                    <a href="shop.html">Strawberry Garden</a>
                  </h3>
                </div>
              </div>
              <div className="single-gallery">
                <img src="assets/img/gallery/4.jpg" alt="Services" />
                <div className="gallery-content">
                  <h3>
                    <a href="shop.html">Strawberry Garden</a>
                  </h3>
                </div>
              </div>
              <div className="single-gallery">
                <img src="assets/img/gallery/5.jpg" alt="Services" />
                <div className="gallery-content">
                  <h3>
                    <a href="shop.html">Strawberry Garden</a>
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
