import React from "react"

export default function BlogArea() {
  return (
    <section className="blog-area ptb-100">
      <div className="container">
        <div className="section-title">
          <span>Blog</span>
          <h2>De notre blog</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
            ipsum suspendisse ultrices gravida.
          </p>
        </div>
        <div className="row">
          <div className="blog-wrap owl-carousel owl-theme">
            <div className="single-blog-post">
              <div className="post-image">
                <a href="#">
                  <img src="assets/img/blog/1.jpg" alt="image" />
                </a>
              </div>
              <div className="post-content">
                <div className="date">
                  <i className="fa fa-calendar"></i>
                  <span>12 Septembre 2019</span>
                </div>
                <h3>
                  <a href="#">
                    Awesome organic food website feature's on the board
                  </a>
                </h3>
                <p>
                  Luis ipsum suspendisse ultrices. Risus commodo viverra
                  maecenas accumsan lacus vel facilisis.
                </p>
                <a className="read-more" href="blog-details.html">
                  Lire plus
                  <i className="flaticon-right"></i>
                </a>
              </div>
            </div>
            <div className="single-blog-post">
              <div className="post-image">
                <a href="#">
                  <img src="assets/img/blog/2.jpg" alt="image" />
                </a>
              </div>
              <div className="post-content">
                <div className="date">
                  <i className="fa fa-calendar"></i>
                  <span>13 October 2019</span>
                </div>
                <h3>
                  <a href="#">Raisons pour lesquelles vous devez choisir l'organique</a>
                </h3>
                <p>
                  Luis ipsum suspendisse ultrices. Risus commodo viverra
                  maecenas accumsan lacus vel facilisis.
                </p>
                <a className="read-more" href="blog-details.html">
                  Lire plus
                  <i className="flaticon-right"></i>
                </a>
              </div>
            </div>
            <div className="single-blog-post">
              <div className="post-image">
                <a href="#">
                  <img src="assets/img/blog/3.jpg" alt="image" />
                </a>
              </div>
              <div className="post-content">
                <div className="date">
                  <i className="fa fa-calendar"></i>
                  <span>14 November 2019</span>
                </div>
                <h3>
                  <a href="#">Soyez en bonne santé, profitez de la vie avec Mamagni</a>
                </h3>
                <p>
                  Luis ipsum suspendisse ultrices. Risus commodo viverra
                  maecenas accumsan lacus vel facilisis.
                </p>
                <a className="read-more" href="blog-details.html">
                  Lire plus
                  <i className="flaticon-right"></i>
                </a>
              </div>
            </div>
            <div className="single-blog-post">
              <div className="post-image">
                <a href="#">
                  <img src="assets/img/blog/4.jpg" alt="image" />
                </a>
              </div>
              <div className="post-content">
                <div className="date">
                  <i className="fa fa-calendar"></i>
                  <span>14 November 2019</span>
                </div>
                <h3>
                  <a href="#">Profitez de votre vie avec un corp en santé</a>
                </h3>
                <p>
                  Luis ipsum suspendisse ultrices. Risus commodo viverra
                  maecenas accumsan lacus vel facilisis.
                </p>
                <a className="read-more" href="blog-details.html">
                  Lire plus
                  <i className="flaticon-right"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="shape shape-1">
        <img src="assets/img/shape/8.png" alt="" />
      </div>
      <div className="shape shape-2">
        <img src="assets/img/shape/9.png" alt="" />
      </div>
    </section>
  )
}
