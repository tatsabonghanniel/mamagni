import React from "react"

const FAQArea = () => {
  return (
    <section className="faq-area">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-6 p-0">
            <div className="faq-img">
              <img src="assets/img/faq-img.jpg" alt="" />
            </div>
          </div>
          <div className="col-lg-6">
            <div className="row">
              <div className="col-lg-12">
                <div className="faq-accordion ptb-100">
                  <h2>Question fréquemment posées</h2>
                  <ul className="accordion">
                    <li className="accordion-item">
                      <a
                        className="accordion-title active"
                        href="javascript:void(0)"
                      >
                        <i className="fa fa-plus"></i>
                        What Do You Eat Orange Food?
                      </a>
                      <p className="accordion-content show">
                        Lorem ipsum dolor, sit amet consectetur adipisicing
                        elit. Quis deleniti nisi necessitatibus, dolores
                        voluptates quam blanditiis fugiat doloremque? Excepturi,
                        minus rem error aut necessitatibus quasi voluptates
                        assumenda ipsum provident tenetur? Lorem ipsum dolor,
                        sit amet consectetur adipisicing elit. Magni nesciunt
                        consectetur sed, tempore, corporis ea maiores libero.
                      </p>
                    </li>
                    <li className="accordion-item">
                      <a className="accordion-title" href="javascript:void(0)">
                        <i className="fa fa-plus"></i>
                        Pourquoi le lait est meilleur pour la santé ?
                      </a>
                      <p className="accordion-content">
                        Lorem ipsum dolor, sit amet consectetur adipisicing
                        elit. Quis deleniti nisi necessitatibus, dolores
                        voluptates quam blanditiis fugiat doloremque? Excepturi,
                        minus rem error aut necessitatibus quasi voluptates
                        assumenda ipsum provident tenetur? Lorem ipsum dolor,
                        sit amet consectetur adipisicing elit. Magni nesciunt
                        consectetur sed, tempore, corporis ea maiores libero.
                      </p>
                    </li>
                    <li className="accordion-item">
                      <a className="accordion-title" href="javascript:void(0)">
                        <i className="fa fa-plus"></i>
                        Bonne alimentation pour une bonne santé
                      </a>
                      <p className="accordion-content">
                        Lorem ipsum dolor, sit amet consectetur adipisicing
                        elit. Quis deleniti nisi necessitatibus, dolores
                        voluptates quam blanditiis fugiat doloremque? Excepturi,
                        minus rem error aut necessitatibus quasi voluptates
                        assumenda ipsum provident tenetur? Lorem ipsum dolor,
                        sit amet consectetur adipisicing elit. Magni nesciunt
                        consectetur sed, tempore, corporis ea maiores libero.
                      </p>
                    </li>
                    <li className="accordion-item">
                      <a className="accordion-title" href="javascript:void(0)">
                        <i className="fa fa-plus"></i>
                        Comment obtenir de la nourriture ?
                      </a>
                      <p className="accordion-content">
                        Lorem ipsum dolor, sit amet consectetur adipisicing
                        elit. Quis deleniti nisi necessitatibus, dolores
                        voluptates quam blanditiis fugiat doloremque? Excepturi,
                        minus rem error aut necessitatibus quasi voluptates
                        assumenda ipsum provident tenetur? Lorem ipsum dolor,
                        sit amet consectetur adipisicing elit. Magni nesciunt
                        consectetur sed, tempore, corporis ea maiores libero.
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default FAQArea
