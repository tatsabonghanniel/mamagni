import React from "react"

export default function PartnerArea() {
  return (
    <div className="partner-area ptb-100">
      <div className="container">
        <div className="partner-wrap owl-carousel owl-theme">
          <div className="partner-item">
            <img src="assets/img/partner/1.png" alt="" />
          </div>
          <div className="partner-item">
            <img src="assets/img/partner/2.png" alt="" />
          </div>
          <div className="partner-item">
            <img src="assets/img/partner/3.png" alt="" />
          </div>
          <div className="partner-item">
            <img src="assets/img/partner/4.png" alt="" />
          </div>
          <div className="partner-item">
            <img src="assets/img/partner/5.png" alt="" />
          </div>
        </div>
      </div>
    </div>
  )
}
