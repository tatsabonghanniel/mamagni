import React from "react"

export default function AboutUsAreaTwo() {
  return (
    <section className="about-us-area about-us-area-two pt-100">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-6">
            <div className="about-content">
              <span>About Us</span>
              <h2>We Provide Fresh Food For Your Family</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
                ipsum suspendisse ultrices gravida. Risus commodo viverra
                maecenas accumsan lacus
              </p>

              <ul>
                <li>
                  <i className="flaticon-check-mark"></i>
                  Premium Quality
                </li>
                <li>
                  <i className="flaticon-check-mark"></i>
                  Harvest Everyday
                </li>
                <li>
                  <i className="flaticon-check-mark"></i>
                  100% Organic
                </li>
                <li>
                  <i className="flaticon-check-mark"></i>
                  Fast Delivery
                </li>
              </ul>
              <a className="default-btn" href="#">
                Learn More
              </a>
            </div>
          </div>
          <div className="col-lg-6">
            <div className="about-img-1">
              <img src="assets/img/about/1.jpg" alt="" />
            </div>
          </div>
        </div>
      </div>
      <div className="shape shape-1">
        <img src="assets/img/shape/3.png" alt="Shape" />
      </div>
      <div className="shape shape-2">
        <img src="assets/img/shape/4.png" alt="Shape" />
      </div>
      <div className="shape shape-3">
        <img src="assets/img/shape/5.png" alt="Shape" />
      </div>
      <div className="shape shape-4">
        <img src="assets/img/shape/6.png" alt="Shape" />
      </div>
      <div className="shape shape-5">
        <img src="assets/img/shape/7.png" alt="Shape" />
      </div>
    </section>
  )
}
