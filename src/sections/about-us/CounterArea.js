import React from "react"

export default function CounterArea() {
  return (
    <section className="counter-area pt-100 pb-70">
      <div className="container">
        <div className="row">
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="single-counter">
              <i className="fa fa-hand-peace-o"></i>
              <h2>
                <span className="odometer" data-count="25">
                  00
                </span>
                +
              </h2>
              <p>Years Experience</p>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="single-counter">
              <i className="fa fa-user-plus"></i>
              <h2>
                <span className="odometer" data-count="85">
                  00
                </span>
                +
              </h2>
              <p>Expart Member</p>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="single-counter">
              <i className="flaticon-plant"></i>
              <h2>
                <span className="odometer" data-count="90">
                  00
                </span>
                +
              </h2>
              <p>Our Product</p>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="single-counter">
              <i className="flaticon-plant"></i>
              <h2>
                <span className="odometer" data-count="270">
                  00
                </span>
                +
              </h2>
              <p>World Wide Branch</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
