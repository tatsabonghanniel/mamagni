import React from "react"

export default function TeamArea() {
  return (
    <section className="team-area ptb-100">
      <div className="container">
        <div className="section-title">
          <span>Team</span>
          <h2>Our Farmer</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
            ipsum suspendisse ultrices gravida.
          </p>
        </div>
        <div className="row">
          <div className="team-wrap owl-carousel owl-theme">
            <div className="single-team">
              <div className="team-img">
                <img src="assets/img/team/1.jpg" alt="" />
                <ul className="team-icon">
                  <li>
                    <a href="#">
                      <i className="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-instagram "></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="team-text">
                <h3>Alvin Pillar</h3>
                <span>SR.Product Engineer</span>
              </div>
            </div>
            <div className="single-team">
              <div className="team-img">
                <img src="assets/img/team/2.jpg" alt="" />
                <ul className="team-icon">
                  <li>
                    <a href="#">
                      <i className="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-instagram "></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="team-text">
                <h3>Frazer Diamond</h3>
                <span>Founder & CEO</span>
              </div>
            </div>
            <div className="single-team">
              <div className="team-img">
                <img src="assets/img/team/3.jpg" alt="" />
                <ul className="team-icon">
                  <li>
                    <a href="#">
                      <i className="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-instagram "></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="team-text">
                <h3>Frazer Diamond</h3>
                <span>SR.Product Engineer</span>
              </div>
            </div>
            <div className="single-team">
              <div className="team-img">
                <img src="assets/img/team/3.jpg" alt="" />
                <ul className="team-icon">
                  <li>
                    <a href="#">
                      <i className="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-instagram "></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="team-text">
                <h3>Frazer Diamond</h3>
                <span>SR.Product Engineer</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
