const React = require("react")

exports.onRenderBody = ({setPostBodyComponents}) => {
          setPostBodyComponents([
            <script type="text/javascript"  src="/assets/js/jquery-3.2.1.slim.min.js"></script>,
            <script type="text/javascript"  src="/assets/js/popper.min.js"></script>,
            <script type="text/javascript"  src="/assets/js/bootstrap.min.js"></script>,
            <script type="text/javascript"  src="/assets/js/jquery.meanmenu.js"></script>,
            <script type="text/javascript"  src="/assets/js/wow.min.js"></script>,
            <script type="text/javascript"  src="/assets/js/owl.carousel.js"></script>,
            <script type="text/javascript"  src="/assets/js/jquery.mixitup.min.js"></script>,
            <script type="text/javascript"  src="/assets/js/jquery.nice-select.min.js"></script>,
            <script type="text/javascript"  src="/assets/js/imagelightbox.min.js"></script>,
            <script type="text/javascript"  src="/assets/js/jquery.appear.js"></script>,
            <script type="text/javascript"  src="/assets/js/odometer.min.js"></script>,
            <script type="text/javascript"  src="/assets/js/rimu-map.js"></script>,
            <script type="text/javascript"  src="/assets/js/custom.js"></script>
    
          ]);
  };